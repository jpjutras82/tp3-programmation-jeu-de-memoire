const validateForm = document.getElementById('valider')
validateForm.addEventListener('click', valider)

function valider(e){
    document.inscription.nombre_paires_requises.style.backgroundColor = 'white'
    document.inscription.nom.style.backgroundColor = 'white'
    const errorMessages = []
    let nombre_paires_requises = document.inscription.nombre_paires_requises.value
    nombre_paires_requises = parseInt(nombre_paires_requises)
    if(isNaN(nombre_paires_requises) || nombre_paires_requises < 0 || nombre_paires_requises > 10) {
        document.inscription.nombre_paires_requises.style.backgroundColor = 'red'
        errorMessages.push('Veuillez entrer un nombre entre 1 et 10 s\'il vous plaît!')
    }

    
    const nom = document.inscription.nom.value
    if(nom === ""){
        document.inscription.nom.style.backgroundColor = 'red'
        errorMessages.push('Vous devez entrer votre nom')
    }

    if (errorMessages.length > 0){
        const ul = document.getElementById('errorMessages')
        ul.innerHTML = ""
        for(let i = 0; i < errorMessages.length; i++){
            const li = document.createElement('li')
            const message = errorMessages[i]
            const texte = document.createTextNode(message)
            
            li.appendChild(texte)
            ul.appendChild(li)
        }
    }
}

const boutonValider = document.getElementById('valider')
boutonValider.addEventListener('click', genererBoutons)


function obtenirParametres(){
    const champNombreBoutons = document.getElementById('nombre-paires-requises')
    let nombreBoutons = champNombreBoutons.value
    nombreBoutons = parseInt(nombreBoutons)

    const champNom = document.getElementById('nom')
    let nom = champNom.value
    nom = parseInt(nom)

    return {nombre1: nom, nombre2: nombreBoutons}
}


function genererBoutons () {
    const nombrePaires = obtenirParametres()
    const tableauCartes = []
    const jeu = document.getElementById('jeu')
    jeu.innerHTML = ""
    for (let i = 0; i < nombrePaires.nombre2; i++) {
    tableauCartes.push(i)
    tableauCartes.push(i)
    }


    while(tableauCartes.length > 0) {
    const index = Math.floor(Math.random() * tableauCartes.length)
    const numerocarte=(tableauCartes[index])
    const bouton = document.createElement('button')
        bouton.style.height = '150px'
        bouton.style.width = '100px'
        bouton.setAttribute('data-nombre-cache', numerocarte)
        bouton.addEventListener('click', clicBouton)
        jeu.appendChild(bouton)
    tableauCartes.splice(index, 1)
    }
}


function clicBouton(e){
    const boutonClique = e.target
    // boutonValider.style.backgroundColor= 'red'
    // alert(boutonClique.textElement)
    const nombre = boutonClique.getAttribute('data-nombre-cache')
    const texte = document.createTextNode(nombre)
    boutonClique.appendChild(texte)
}




//ALLER CHERCHER NOM dans le formulaire (avec name): document.formulaire/inscription.nom.value

/*//Validation
const form = document.getElementById('creation-partie')
form.addEventListener('submit', valider)

function valider() {
    const paire = document.getElementById('nombre-paires-requises').value
    paire = parseInt(paire)
    if(paire < 0){
        alert('paire inférieur à 0')
        erreur = true
    }
    if (erreur) {
        e.preventDefault()
    }
} */

//ALLER CHERCHER NOM dans le formulaire (avec name): document.formulaire/inscription.nom.value


/*  BACKUP GENERERBOUTONS
function genererBoutons () {
    const parametres = obtenirParametres()
    const jeu = document.getElementById('jeu')
    jeu.innerHTML = ""
    for(let i = 0; i < parametres.nombre2; i++) {
        const bouton = document.createElement('button')
        bouton.style.height = '150px'
        bouton.style.width = '100px'
        bouton.setAttribute('data-nombre-cache', 4)
        bouton.addEventListener('click', clicBouton)
        jeu.appendChild(bouton)
    }
}
*/
